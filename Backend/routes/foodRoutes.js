import express from "express";
import { addFood, foodList, foodRemove } from "../controllers/foodControllers.js";

//create image storage system

import multer from "multer";

//creating router
const foodRouter = express.Router();

//image storage engine
const storage = multer.diskStorage({
  destination: "uploads",
  filename: (req, file, cb) => {
    return cb(null, `${Date.now()}${file.originalname}`);
  },
});

//creating the upload middleware
const upload = multer({ storage: storage });

foodRouter.post("/add", upload.single("image"), addFood);
foodRouter.get("/list" , foodList)
foodRouter.post("/remove",foodRemove)

export default foodRouter;
