import express from "express";
import cors from "cors";
import { connectDB } from "./config/db.js";
import foodRouter from "./routes/foodRoutes.js";

//app config
const app = express();
const port = 4000;

//middleawre
app.use(express.json());
app.use(cors());

// db connection
connectDB();

//api end point
app.use("/api/food", foodRouter);

app.get("/", (req, res) => {
  res.send("API working");
});

app.listen(port, () => {
  console.log(`sever started on http://localhost:${port}`);
});
